clear
clc
close all
%%
M = 2^10;
x = 0:1/M:1-1/M;
K = 1000;
sigma = 0.3;

rcoeff = 0.1:0.025:1;

W3db.Rect = zeros(1,length(rcoeff));
W3db.HFT95 = zeros(1,length(rcoeff));
W3db.Gauss = zeros(1,length(rcoeff));
W3db.Nuttall4a = zeros(1,length(rcoeff));
W3db.HFT95 = zeros(1,length(rcoeff));

emax.Rect = zeros(1,length(rcoeff));
emax.HFT95 = zeros(1,length(rcoeff));
emax.Gauss = zeros(1,length(rcoeff));
emax.Nuttall4a = zeros(1,length(rcoeff));
emax.HFT95 = zeros(1,length(rcoeff));

PSLL.Rect = zeros(1,length(rcoeff));
PSLL.HFT95 = zeros(1,length(rcoeff));
PSLL.Gauss = zeros(1,length(rcoeff));
PSLL.Nuttall4a = zeros(1,length(rcoeff));
PSLL.HFT95 = zeros(1,length(rcoeff));

NENBW.Rect = zeros(1,length(rcoeff));
NENBW.HFT95 = zeros(1,length(rcoeff));
NENBW.Gauss = zeros(1,length(rcoeff));
NENBW.Nuttall4a = zeros(1,length(rcoeff));
NENBW.HFT95 = zeros(1,length(rcoeff));

ROV.Rect = zeros(1,length(rcoeff));
ROV.HFT95 = zeros(1,length(rcoeff));
ROV.Gauss = zeros(1,length(rcoeff));
ROV.Nuttall4a = zeros(1,length(rcoeff));
ROV.HFT95 = zeros(1,length(rcoeff));


for n=1:length(rcoeff)
    
    r = rcoeff(n);
    
    %������������� ����
    N = round(r*M);
    
    
    Rect.W = [ones(1,N),zeros(1,M-N)];
    [Rect.W3db, Rect.emax, Rect.PSLL, Rect.S, Rect.NENBW, Rect.r, Rect.AF, Rect.PF, Rect.OC, Rect.S1, Rect.S2, Rect.ROV] = getWindowProperties(Rect.W,M,K);
    
    z = zeros(1,length(x));
    HFT95.W = zeros(1,length(x));
    for i=1:length(x)
        if(x(i)<r)
            z(i) = 2*pi.*x(i)/r;
        else
            break;
        end
    end
    %���� HFT95
    c0 = 1;
    c1 = -1.9383379;
    c2 = 1.3045202;
    c3 = -0.4028270;
    c4 = 0.0350665;
    
    HFT95.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);
    
    [HFT95.W3db, HFT95.emax, HFT95.PSLL, HFT95.S, HFT95.NENBW, HFT95.r, HFT95.AF, HFT95.PF, HFT95.OC, HFT95.S1, HFT95.S2, HFT95.ROV] = getWindowProperties(HFT95.W,M,K);
    %���� ������
    Gauss.W = zeros(1,length(x));
    for i=1:length(x)
        if(x(i)<r)
            
            Gauss.W(i) = exp(-1/(2*sigma) * (2*x(i)/r-1)^2);
            
        else
            break;
        end
    end
    
    [Gauss.W3db, Gauss.emax, Gauss.PSLL, Gauss.S, Gauss.NENBW, Gauss.r, Gauss.AF, Gauss.PF, Gauss.OC, Gauss.S1, Gauss.S2, Gauss.ROV] = getWindowProperties(Gauss.W,M,K);
    %���� Nuttall4a
    c0 = 0.338946;
    c1 = -0.481973;
    c2 = 0.161054;
    c3 = -0.018027;
    
    Nuttall4a.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);
    [Nuttall4a.W3db, Nuttall4a.emax, Nuttall4a.PSLL, Nuttall4a.S, Nuttall4a.NENBW, Nuttall4a.r, Nuttall4a.AF, Nuttall4a.PF, Nuttall4a.OC, Nuttall4a.S1, Nuttall4a.S2, Nuttall4a.ROV] = getWindowProperties(Nuttall4a.W,M,K);
    
    
    
    %W3db
    W3db.Rect(n) = Rect.W3db;
    W3db.HFT95(n) = HFT95.W3db;
    W3db.Gauss(n) = Gauss.W3db;
    W3db.Nuttall4a(n) = Nuttall4a.W3db;
    %emax
    emax.Rect(n) = Rect.emax;
    emax.HFT95(n) = HFT95.emax;
    emax.Gauss(n) = Gauss.emax;
    emax.Nuttall4a(n) = Nuttall4a.emax;
    %PSLL
    PSLL.Rect(n) = Rect.PSLL;
    PSLL.HFT95(n) = HFT95.PSLL;
    PSLL.Gauss(n) = Gauss.PSLL;
    PSLL.Nuttall4a(n) = Nuttall4a.PSLL;
    %NENBW
    NENBW.Rect(n) = Rect.NENBW;
    NENBW.HFT95(n) = HFT95.NENBW;
    NENBW.Gauss(n) = Gauss.NENBW;
    NENBW.Nuttall4a(n) = Nuttall4a.NENBW;
    %ROV
    ROV.Rect(n) = Rect.ROV;
    ROV.HFT95(n) = HFT95.ROV;
    ROV.Gauss(n) = Gauss.ROV;
    ROV.Nuttall4a(n) = Nuttall4a.ROV;
    
    
end

figure;
plot(rcoeff,W3db.Rect);
hold on;
plot(rcoeff,W3db.HFT95);
hold on;
plot(rcoeff,W3db.Gauss);
hold on;
plot(rcoeff,W3db.Nuttall4a);
hold on;
legend('Rect','HFT95','Gauss','Nuttall4a');
xlabel('r');
ylabel('W3db');

figure;
plot(rcoeff,emax.Rect);
hold on;
plot(rcoeff,emax.HFT95);
hold on;
plot(rcoeff,emax.Gauss);
hold on;
plot(rcoeff,emax.Nuttall4a);
hold on;
legend('Rect','HFT95','Gauss','Nuttall4a');
xlabel('r');
ylabel('emax');


figure;
plot(rcoeff,PSLL.Rect);
hold on;
plot(rcoeff,PSLL.HFT95);
hold on;
plot(rcoeff,PSLL.Gauss);
hold on;
plot(rcoeff,PSLL.Nuttall4a);
hold on;
legend('Rect','HFT95','Gauss','Nuttall4a');
xlabel('r');
ylabel('PSLL');

figure;
plot(rcoeff,NENBW.Rect);
hold on;
plot(rcoeff,NENBW.HFT95);
hold on;
plot(rcoeff,NENBW.Gauss);
hold on;
plot(rcoeff,NENBW.Nuttall4a);
hold on;
legend('Rect','HFT95','Gauss','Nuttall4a');
xlabel('r');
ylabel('NENBW');

figure;
plot(rcoeff,ROV.Rect);
hold on;
plot(rcoeff,ROV.HFT95);
hold on;
plot(rcoeff,ROV.Gauss);
hold on;
plot(rcoeff,ROV.Nuttall4a);
legend('Rect','HFT95','Gauss','Nuttall4a');
xlabel('r');
ylabel('ROV');