function [r,AF,PF,OC,ROV] = getOverlapParameters(W,N)


r = 0:0.1:99; %overlaps
AF = zeros(1,length(r));
PF = zeros(1,length(r));
OC = zeros(1,length(r));

shifts = 100 - r;
for i = 1:length(shifts)
   
    shift = shifts(i); %�������� ������� ����� (� %)
    %����� ������ ��������
    samps = (-N:1:2*N-1) + N;
    %����� (� �������)
    k = round(shift/100 * N);
    
    kW = floor(N/k);
    
    winds = zeros(2*kW, length(samps));
    
    wind0 = zeros(1,length(samps));
    
    wind0(N+1:2*N) = W;
    
    for j=1:kW
        
        winds(j,N+1-j*k:2*N-j*k) = W;
        winds(kW+j,N+1+j*k:2*N+j*k) = W;
        
    end
    
    w = sum(winds) + wind0;
    w2 = sum(winds.^2) + wind0.^2;
    w = w./max(w);
    w2 = w2./max(w2);
    
    AF(i) = (1-(max(w(N+1:2*N)) - min(w(N+1:2*N))));
    PF(i) = sqrt(1- (max(w2(N+1:2*N)) - min(w2(N+1:2*N)))   );
    
    
end
%���������� OC
%ws = repmat(W,3,1);
for i=1:length(r)
    
    r1 = r(i)/100;
    
    summ = 0;
    for j=1:r1*N
        
        summ = summ + W(j) * W(j+round((1-r1)*N));
        
    end
    
    OC(i) = summ/sum(W.^2);
    
    
end

%��������� ROV
d = AF - OC;
[~, idx] = max(d);
ROV = r(idx);

end