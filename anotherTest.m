clear
clc
close all

M = 2^10;
r = 1; 
x = 0:1/M:1-1/M;
K = 1000;

z = zeros(1,length(x));
N = round(r*M);
offset = round((M-N)/2);

for i=1:length(x)
    if(x(i)<r)
        z(i+offset) = 2*pi.*x(i)/r;
    else
        break;
    end
end

%���� FTHP
c0 = 1.0;
c1 = -1.912510941;
c2 = 1.079173272;
c3 = -0.1832630879;

FTHP.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[FTHP.W3db, FTHP.emax, FTHP.PSLL, FTHP.S, FTHP.NENBW, FTHP.r, FTHP.AF, FTHP.PF, FTHP.OC, FTHP.S1, FTHP.S2, FTHP.ROV] = getWindowProperties(FTHP.W,M,K);
isDraw = drawWind(FTHP,M,K,'FTHP',[-100, 0]);