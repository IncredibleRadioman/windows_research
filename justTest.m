%%
clc
close all
clear
%���� �����
N = 2^5;
K = 1000;
L = K*N;
M = 2^10;
j = 1:N;
%%
buf_data = 1:1024;
buf_valid = repmat([1,1,1,0],1,256);
inp_signal.time = [];
inp_signal.signals.values=buf_data.';
inp_signal.signals.dimensions=1;

inp_valid.time = [];
inp_valid.signals.values=buf_valid.';
inp_valid.signals.dimensions=1;

wtype.time = [];
wtype.signals.values=[0, 0].';
wtype.signals.dimensions=1;

wlen.time = [];
wlen.signals.values=[15, 15].';
wlen.signals.dimensions=1;

wlen_inv.time = [];
wlen_inv.signals.values=[1/16, 1/16].';
wlen_inv.signals.dimensions=1;