function isDraw = drawWind(Wind,N,K,text,loglim)



figure;
set(0,'DefaultAxesFontSize',14,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',14,'DefaultTextFontName','Times New Roman');
subplot(3,2,1);
plot((0:N-1)./N,Wind.W.*(N/Wind.S1));
xlabel('index j/N');
ylabel('normalized window value');
title(text);
grid on;

subplot(3,2,2);
str = strcat('NENBW = ',num2str(Wind.NENBW),' bins');
plot((-K*N/2:K*N/2-1)./K,fftshift(Wind.S));
xlabel('bins');
ylabel('amplitude, dB');
xlim([-50, 50]);
ylim([-80, 2]);
grid on;
title(str);

subplot(3,2,3);
str = strcat('W3db = ',num2str(Wind.W3db),' bins, PSLL = ',num2str(Wind.PSLL),' dB');
plot((-K*N/2:K*N/2-1)./K,fftshift(Wind.S));
xlabel('bins');
ylabel('amplitude, dB');
xlim([-4*abs(Wind.W3db), 4*abs(Wind.W3db)]);
ylim([-3*abs(Wind.PSLL), 2]);
grid on;
title(str);

subplot(3,2,4);
str = strcat('emax = ',num2str(Wind.emax),' dB');
plot((-K*N/2:K*N/2-1)./K,fftshift(Wind.S));
xlabel('bins');
ylabel('amplitude, dB');
xlim([-0.5, 0.5]);
ylim([-abs(Wind.emax), abs(Wind.emax)]);
grid on;
title(str);

subplot(3,2,5);
semilogx((0:K*N/2-1)./K,Wind.S(1:length(0:K*N/2-1)));
ylim([-180, -30]);
n = ceil(log10(N/2));
xlim([10, 10^n]);
ylim(loglim);
xlabel('bins, LOG');
ylabel('amplitude, dB');
grid on;

subplot(3,2,6);
plot(Wind.r,Wind.AF);
str = strcat('ROV = ',num2str(Wind.ROV),'%');
hold on;
plot(Wind.r,Wind.PF);
hold on;
plot(Wind.r,Wind.OC);
xlabel('overlap, %');
ylabel('flatness, correlation');
xlim([0, 100]);
ylim([-0.4, 1]);
grid on;
title(str);

isDraw = 1;

end