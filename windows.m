clear
clc 
close all
%%
%������������ ������� �������
%����� ����� �����
M = 2^10; 
%����������� ����������
r = 0.887; 
%������ ���������
x = 0:1/M:1-1/M;

N = round(r*M);

K = 1000;
L = K*N;
%%
%������������� ���� (������������ ��� �������)
Rect.W = [ones(1,N),zeros(1,M-N)];
[Rect.W3db, Rect.emax, Rect.PSLL, Rect.S, Rect.NENBW, Rect.r, Rect.AF, Rect.PF, Rect.OC, Rect.S1, Rect.S2, Rect.ROV] = getWindowProperties(Rect.W,M,K);
isDraw = drawWind(Rect,M,K,'Rect',[-70, 0]);
%%
%���� ���������
Bart.W = zeros(1,M);
for i=1:length(x)
    
    if(x(i)<r)
        z = 2*x(i)/r;
    else
        z = 0;
    end
    
    if (z<=1)
        Bart.W(i) = z;
    else
        Bart.W(i) = 2-z;
    end
    
end

[Bart.W3db, Bart.emax, Bart.PSLL, Bart.S, Bart.NENBW, Bart.r, Bart.AF, Bart.PF, Bart.OC, Bart.S1, Bart.S2, Bart.ROV] = getWindowProperties(Bart.W,M,K);
isDraw = drawWind(Bart,M,K,'Bartlett',[-140, 0]);
%%
%���� �����
z = zeros(1,M);
for i=1:length(x)
    if(x(i)<r)
        z(i) = 2*x(i)/r;
    else
        z(i) = 0;
    end
end
Welch.W = 1 - (z-1).^2;

[Welch.W3db, Welch.emax, Welch.PSLL, Welch.S, Welch.NENBW, Welch.r, Welch.AF, Welch.PF, Welch.OC, Welch.S1, Welch.S2, Welch.ROV] = getWindowProperties(Welch.W,M,K);
isDraw = drawWind(Welch,M,K,'Welch',[-140, 0]);
%%
%���� �����
for i=1:length(x)
    if(x(i)<r)
        z(i) = 2*pi.*x(i)/r;
    else
        z(i) = 0;
    end
end
Hann.W = (1-cos(z))./2;

[Hann.W3db, Hann.emax, Hann.PSLL, Hann.S, Hann.NENBW, Hann.r, Hann.AF, Hann.PF, Hann.OC, Hann.S1, Hann.S2, Hann.ROV] = getWindowProperties(Hann.W,M,K);
isDraw = drawWind(Hann,M,K,'Hanning',[-220, 0]);
%%
%���� ��������
Hamm.W = 0.54 - 0.46.*cos(z);

[Hamm.W3db, Hamm.emax, Hamm.PSLL, Hamm.S, Hamm.NENBW, Hamm.r, Hamm.AF, Hamm.PF, Hamm.OC, Hamm.S1, Hamm.S2, Hamm.ROV] = getWindowProperties(Hamm.W,M,K);
isDraw = drawWind(Hamm,M,K,'Hamming',[-100,0]);
%%
%���� ��������-�������
BlackHarr.W = 0.35875 - 0.48829.*cos(z) + 0.14128.*cos(2.*z) - 0.01168.*cos(3.*z);
[BlackHarr.W3db, BlackHarr.emax, BlackHarr.PSLL, BlackHarr.S, BlackHarr.NENBW, BlackHarr.r, BlackHarr.AF, BlackHarr.PF, BlackHarr.OC, BlackHarr.S1, BlackHarr.S2, BlackHarr.ROV] = getWindowProperties(BlackHarr.W,M,K);
isDraw = drawWind(BlackHarr,M,K,'BH92',[-160,0]);
%%
%���� Nuttall3
c0 = 0.375;
c1 = -0.5;
c2 = 0.125;
c3 = 0;

Nuttall3.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall3.W3db, Nuttall3.emax, Nuttall3.PSLL, Nuttall3.S, Nuttall3.NENBW, Nuttall3.r, Nuttall3.AF, Nuttall3.PF, Nuttall3.OC, Nuttall3.S1, Nuttall3.S2, Nuttall3.ROV] = getWindowProperties(Nuttall3.W,M,K);
isDraw = drawWind(Nuttall3,M,K,'Nuttall3',[-350,0]);
%%
%���� Nuttall3a
c0 = 0.40897;
c1 = -0.5;
c2 = 0.09103;
c3 = 0;

Nuttall3a.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall3a.W3db, Nuttall3a.emax, Nuttall3a.PSLL, Nuttall3a.S, Nuttall3a.NENBW, Nuttall3a.r, Nuttall3a.AF, Nuttall3a.PF, Nuttall3a.OC, Nuttall3a.S1, Nuttall3a.S2, Nuttall3a.ROV] = getWindowProperties(Nuttall3a.W,M,K);
isDraw = drawWind(Nuttall3a,M,K,'Nuttall3a',[-260,0]);
%%
%���� Nuttall3b
c0 = 0.4243801;
c1 = -0.4973406;
c2 = 0.0782793;
c3 = 0;

Nuttall3b.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall3b.W3db, Nuttall3b.emax, Nuttall3b.PSLL, Nuttall3b.S, Nuttall3b.NENBW, Nuttall3b.r, Nuttall3b.AF, Nuttall3b.PF, Nuttall3b.OC, Nuttall3b.S1, Nuttall3b.S2, Nuttall3b.ROV] = getWindowProperties(Nuttall3b.W,M,K);
isDraw = drawWind(Nuttall3b,M,K,'Nuttall3b',[-120,0]);
%%
%���� Nuttall4
c0 = 0.3125;
c1 = -0.46875;
c2 = 0.1875;
c3 = -0.03125;

Nuttall4.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall4.W3db, Nuttall4.emax, Nuttall4.PSLL, Nuttall4.S, Nuttall4.NENBW, Nuttall4.r, Nuttall4.AF, Nuttall4.PF, Nuttall4.OC, Nuttall4.S1, Nuttall4.S2, Nuttall4.ROV] = getWindowProperties(Nuttall4.W,M,K);
isDraw = drawWind(Nuttall4,M,K,'Nuttall4',[-400,0]);
%%
%���� Nuttall4a
c0 = 0.338946;
c1 = -0.481973;
c2 = 0.161054;
c3 = -0.018027;

Nuttall4a.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall4a.W3db, Nuttall4a.emax, Nuttall4a.PSLL, Nuttall4a.S, Nuttall4a.NENBW, Nuttall4a.r, Nuttall4a.AF, Nuttall4a.PF, Nuttall4a.OC, Nuttall4a.S1, Nuttall4a.S2, Nuttall4a.ROV] = getWindowProperties(Nuttall4a.W,M,K);
isDraw = drawWind(Nuttall4a,M,K,'Nuttall4a',[-400,0]);
%%
%���� Nuttall4b
c0 = 0.355768;
c1 = -0.487396;
c2 = 0.144232;
c3 = -0.012604;

Nuttall4b.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall4b.W3db, Nuttall4b.emax, Nuttall4b.PSLL, Nuttall4b.S, Nuttall4b.NENBW, Nuttall4b.r, Nuttall4b.AF, Nuttall4b.PF, Nuttall4b.OC, Nuttall4b.S1, Nuttall4b.S2, Nuttall4b.ROV] = getWindowProperties(Nuttall4b.W,M,K);
isDraw = drawWind(Nuttall4b,M,K,'Nuttall4b',[-260,0]);
%%
%���� Nuttall4c
c0 = 0.3635819;
c1 = -0.4891775;
c2 = 0.1365995;
c3 = -0.0106411;

Nuttall4c.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[Nuttall4c.W3db, Nuttall4c.emax, Nuttall4c.PSLL, Nuttall4c.S, Nuttall4c.NENBW, Nuttall4c.r, Nuttall4c.AF, Nuttall4c.PF, Nuttall4c.OC, Nuttall4c.S1, Nuttall4c.S2, Nuttall4c.ROV] = getWindowProperties(Nuttall4c.W,M,K);
isDraw = drawWind(Nuttall4c,M,K,'Nuttall4c',[-140,0]);
%%
%���� ������� (a = 3)
a = 3;
for i=1:length(x)
    if(x(i)<r)
        z(i) = 2*x(i)/r-1;
    else
        z(i) = 0;
    end
end

Kaiser3.W = besseli(0,pi*a.*sqrt(1-z.^2))./besseli(0,pi*a);

[Kaiser3.W3db, Kaiser3.emax, Kaiser3.PSLL, Kaiser3.S, Kaiser3.NENBW, Kaiser3.r, Kaiser3.AF, Kaiser3.PF, Kaiser3.OC, Kaiser3.S1, Kaiser3.S2, Kaiser3.ROV] = getWindowProperties(Kaiser3.W,M,K);
isDraw = drawWind(Kaiser3,M,K,'Kaiser3',[-140,0]);
%%
%���� ������� (a = 4)
a = 4;

Kaiser4.W = besseli(0,pi*a.*sqrt(1-z.^2))./besseli(0,pi*a);

[Kaiser4.W3db, Kaiser4.emax, Kaiser4.PSLL, Kaiser4.S, Kaiser4.NENBW, Kaiser4.r, Kaiser4.AF, Kaiser4.PF, Kaiser4.OC, Kaiser4.S1, Kaiser4.S2, Kaiser4.ROV] = getWindowProperties(Kaiser4.W,M,K);
isDraw = drawWind(Kaiser4,M,K,'Kaiser4',[-160,0]);
%%
%���� ������� (a = 5)
a = 5;

Kaiser5.W = besseli(0,pi*a.*sqrt(1-z.^2))./besseli(0,pi*a);

[Kaiser5.W3db, Kaiser5.emax, Kaiser5.PSLL, Kaiser5.S, Kaiser5.NENBW, Kaiser5.r, Kaiser5.AF, Kaiser5.PF, Kaiser5.OC, Kaiser5.S1, Kaiser5.S2, Kaiser5.ROV] = getWindowProperties(Kaiser5.W,M,K);
isDraw = drawWind(Kaiser5,M,K,'Kaiser5',[-190,0]);
%%
%���� SFT3F
for i=1:length(x)
    if(x(i)<r)
        z(i) = 2*pi.*x(i)/r;
    else
        z(i) = 0;
    end
end
c0 = 0.26526;
c1 = -0.5;
c2 = 0.23474;
c3 = 0;
c4 = 0;

SFT3F.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[SFT3F.W3db, SFT3F.emax, SFT3F.PSLL, SFT3F.S, SFT3F.NENBW, SFT3F.r, SFT3F.AF, SFT3F.PF, SFT3F.OC, SFT3F.S1, SFT3F.S2, SFT3F.ROV] = getWindowProperties(SFT3F.W,M,K);
isDraw = drawWind(SFT3F,M,K,'SFT3F',[-220,0]);
%%
%���� SFT4F
c0 = 0.21706;
c1 = -0.42103;
c2 = 0.28294;
c3 = -0.07897;
c4 = 0;

SFT4F.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[SFT4F.W3db, SFT4F.emax, SFT4F.PSLL, SFT4F.S, SFT4F.NENBW, SFT4F.r, SFT4F.AF, SFT4F.PF, SFT4F.OC, SFT4F.S1, SFT4F.S2, SFT4F.ROV] = getWindowProperties(SFT4F.W,M,K);
isDraw = drawWind(SFT4F,M,K,'SFT4F',[-350,0]);
%%
%���� SFT5F
c0 = 0.1881;
c1 = -0.36923;
c2 = 0.28702;
c3 = -0.13077;
c4 = 0.02488;

SFT5F.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[SFT5F.W3db, SFT5F.emax, SFT5F.PSLL, SFT5F.S, SFT5F.NENBW, SFT5F.r, SFT5F.AF, SFT5F.PF, SFT5F.OC, SFT5F.S1, SFT5F.S2, SFT5F.ROV] = getWindowProperties(SFT5F.W,M,K);
isDraw = drawWind(SFT5F,M,K,'SFT5F',[-400,0]);
%%
%���� SFT3M
c0 = 0.28235;
c1 = -0.52105;
c2 = 0.19659;
c3 = 0;
c4 = 0;

SFT3M.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[SFT3M.W3db, SFT3M.emax, SFT3M.PSLL, SFT3M.S, SFT3M.NENBW, SFT3M.r, SFT3M.AF, SFT3M.PF, SFT3M.OC, SFT3M.S1, SFT3M.S2, SFT3M.ROV] = getWindowProperties(SFT3M.W,M,K);
isDraw = drawWind(SFT3M,M,K,'SFT3M',[-100,0]);
%%
%���� SFT4M
c0 = 0.241906;
c1 = -0.460841;
c2 = 0.2553381;
c3 = -0.041872;
c4 = 0;

SFT4M.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[SFT4M.W3db, SFT4M.emax, SFT4M.PSLL, SFT4M.S, SFT4M.NENBW, SFT4M.r, SFT4M.AF, SFT4M.PF, SFT4M.OC, SFT4M.S1, SFT4M.S2, SFT4M.ROV] = getWindowProperties(SFT4M.W,M,K);
isDraw = drawWind(SFT4M,M,K,'SFT4M',[-120,0]);
%%
%���� SFT5M
c0 = 0.209671;
c1 = -0.407331;
c2 = 0.281225;
c3 = -0.092669;
c4 = 0.0091036;

SFT5M.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[SFT5M.W3db, SFT5M.emax, SFT5M.PSLL, SFT5M.S, SFT5M.NENBW, SFT5M.r, SFT5M.AF, SFT5M.PF, SFT5M.OC, SFT5M.S1, SFT5M.S2, SFT5M.ROV] = getWindowProperties(SFT5M.W,M,K);
isDraw = drawWind(SFT5M,M,K,'SFT5M',[-220,0]);
%%
%National Instruments window (FTNI)
c0 = 0.2810639;
c1 = -0.5208972;
c2 = 0.1980399;

FTNI.W = c0 + c1.*cos(z) + c2.*cos(2.*z);

[FTNI.W3db, FTNI.emax, FTNI.PSLL, FTNI.S, FTNI.NENBW, FTNI.r, FTNI.AF, FTNI.PF, FTNI.OC, FTNI.S1, FTNI.S2, FTNI.ROV] = getWindowProperties(FTNI.W,M,K);
isDraw = drawWind(FTNI,M,K,'FTNI',[-100,0]);
%%
%���� FTHP
c0 = 1.0;
c1 = -1.912510941;
c2 = 1.079173272;
c3 = -0.1832630879;

FTHP.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[FTHP.W3db, FTHP.emax, FTHP.PSLL, FTHP.S, FTHP.NENBW, FTHP.r, FTHP.AF, FTHP.PF, FTHP.OC, FTHP.S1, FTHP.S2, FTHP.ROV] = getWindowProperties(FTHP.W,M,K);
isDraw = drawWind(FTHP,M,K,'FTHP',[-120,0]);
%%
%FTSRS
c0 = 1.0;
c1 = -1.93;
c2 = 1.29;
c3 = -0.388;
c4 = 0.028;

FTSRS.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[FTSRS.W3db, FTSRS.emax, FTSRS.PSLL, FTSRS.S, FTSRS.NENBW, FTSRS.r, FTSRS.AF, FTSRS.PF, FTSRS.OC, FTSRS.S1, FTSRS.S2, FTSRS.ROV] = getWindowProperties(FTSRS.W,M,K);
isDraw = drawWind(FTSRS,M,K,'FTSRS',[-240,0]);
%%
%HFT70
c0 = 1;
c1 = -1.907961;
c2 = 1.07349;
c3 = -0.18199;

HFT70.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z);

[HFT70.W3db, HFT70.emax, HFT70.PSLL, HFT70.S, HFT70.NENBW, HFT70.r, HFT70.AF, HFT70.PF, HFT70.OC, HFT70.S1, HFT70.S2, HFT70.ROV] = getWindowProperties(HFT70.W,M,K);
isDraw = drawWind(HFT70,M,K,'HFT70',[-120,0]);
%%
%HFT95
c0 = 1;
c1 = -1.9383379;
c2 = 1.3045202;
c3 = -0.4028270;
c4 = 0.0350665;

HFT95.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[HFT95.W3db, HFT95.emax, HFT95.PSLL, HFT95.S, HFT95.NENBW, HFT95.r, HFT95.AF, HFT95.PF, HFT95.OC, HFT95.S1, HFT95.S2, HFT95.ROV] = getWindowProperties(HFT95.W,M,K);
isDraw = drawWind(HFT95,M,K,'HFT95',[-140,0]);
%%
%HFT90D
c0 = 1;
c1 = -1.942604;
c2 = 1.340318;
c3 = -0.440811;
c4 = 0.043097;

HFT90D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z);

[HFT90D.W3db, HFT90D.emax, HFT90D.PSLL, HFT90D.S, HFT90D.NENBW, HFT90D.r, HFT90D.AF, HFT90D.PF, HFT90D.OC, HFT90D.S1, HFT90D.S2, HFT90D.ROV] = getWindowProperties(HFT90D.W,M,K);
isDraw = drawWind(HFT90D,M,K,'HFT90D',[-240,0]);
%%
%HFT116D
c0 = 1;
c1 = -1.9575375;
c2 = 1.4780705;
c3 = -0.6367431;
c4 = 0.1228389;
c5 = -0.0066288;

HFT116D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z) + c5.*cos(5.*z);

[HFT116D.W3db, HFT116D.emax, HFT116D.PSLL, HFT116D.S, HFT116D.NENBW, HFT116D.r, HFT116D.AF, HFT116D.PF, HFT116D.OC, HFT116D.S1, HFT116D.S2, HFT116D.ROV] = getWindowProperties(HFT116D.W,M,K);
isDraw = drawWind(HFT116D,M,K,'HFT116D',[-260,0]);
%%
%HFT144D
c0 = 1;
c1 = -1.96760033;
c2 = 1.57983607;
c3 = -0.81123644;
c4 = 0.22583558;
c5 = -0.02773848;
c6 = 0.00090360;

HFT144D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z) + c5.*cos(5.*z) + c6.*cos(6.*z);

[HFT144D.W3db, HFT144D.emax, HFT144D.PSLL, HFT144D.S, HFT144D.NENBW, HFT144D.r, HFT144D.AF, HFT144D.PF, HFT144D.OC, HFT144D.S1, HFT144D.S2, HFT144D.ROV] = getWindowProperties(HFT144D.W,M,K);
isDraw = drawWind(HFT144D,M,K,'HFT144D',[-280,0]);
%%
%HFT169D
c0 = 1;
c1 = -1.97441842;
c2 = 1.65409888;
c3 = -0.95788186;
c4 = 0.33673420;
c5 = -0.06364621;
c6 = 0.00521942;
c7 = -0.00010599;

HFT169D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z) + c5.*cos(5.*z) + c6.*cos(6.*z) + c7.*cos(7.*z);

[HFT169D.W3db, HFT169D.emax, HFT169D.PSLL, HFT169D.S, HFT169D.NENBW, HFT169D.r, HFT169D.AF, HFT169D.PF, HFT169D.OC, HFT169D.S1, HFT169D.S2, HFT169D.ROV] = getWindowProperties(HFT169D.W,M,K);
isDraw = drawWind(HFT169D,M,K,'HFT169D',[-300,0]);
%%
%HFT196D
c0 = 1;
c1 = -1.979280420;
c2 = 1.710288951;
c3 = -1.081629853;
c4 = 0.448734314;
c5 = -0.112376628;
c6 = 0.015122992;
c7 = -0.000871252;
c8 = 0.000011896;

HFT196D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z) + c5.*cos(5.*z) + c6.*cos(6.*z) + c7.*cos(7.*z) + c8.*cos(8.*z);

[HFT196D.W3db, HFT196D.emax, HFT196D.PSLL, HFT196D.S, HFT196D.NENBW, HFT196D.r, HFT196D.AF, HFT196D.PF, HFT196D.OC, HFT196D.S1, HFT196D.S2, HFT196D.ROV] = getWindowProperties(HFT196D.W,M,K);
isDraw = drawWind(HFT196D,M,K,'HFT196D',[-320,0]);
%%
%HFT223D
c0 = 1;
c1 = -1.98298997309;
c2 = 1.75556083063;
c3 = -1.19037717712;
c4 = 0.56155440797;
c5 = -0.17296769663;
c6 = 0.03233247087;
c7 = -0.00324954578;
c8 = 0.00013801040;
c9 = -0.00000132725;

HFT223D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z) + c5.*cos(5.*z) + c6.*cos(6.*z) + c7.*cos(7.*z) + c8.*cos(8.*z) + c9.*cos(9.*z);

[HFT223D.W3db, HFT223D.emax, HFT223D.PSLL, HFT223D.S, HFT223D.NENBW, HFT223D.r, HFT223D.AF, HFT223D.PF, HFT223D.OC, HFT223D.S1, HFT223D.S2, HFT223D.ROV] = getWindowProperties(HFT223D.W,M,K);
isDraw = drawWind(HFT223D,M,K,'HFT223D',[-320,0]);
%%
%HFT248D
c0 = 1;
c1 = -1.985844164102;
c2 = 1.791176438506;
c3 = -1.282075284005;
c4 = 0.667777530266;
c5 = -0.240160796576;
c6 = 0.056656381764;
c7 = -0.008134974479;
c8 = 0.000624544650;
c9 = -0.000019808998;
c10 = 0.000000132974;

HFT248D.W = c0 + c1.*cos(z) + c2.*cos(2.*z) + c3.*cos(3.*z) + c4.*cos(4.*z) + c5.*cos(5.*z) + c6.*cos(6.*z) + c7.*cos(7.*z) + c8.*cos(8.*z) + c9.*cos(9.*z) + c10.*cos(10.*z);

[HFT248D.W3db, HFT248D.emax, HFT248D.PSLL, HFT248D.S, HFT248D.NENBW, HFT248D.r, HFT248D.AF, HFT248D.PF, HFT248D.OC, HFT248D.S1, HFT248D.S2, HFT248D.ROV] = getWindowProperties(HFT248D.W,M,K);
isDraw = drawWind(HFT248D,M,K,'HFT248D',[-320,0]);