%%
clc
clear
close all
%%
%������ ��������� ������� ����
N = 128; %����� ����
%����������� ���������� �� ���������, ��� ��� =1
%���� �������� ��������: W = c0 + c1.*cos(z) + c2.*cos(2.*z);
%��� z = 2 * pi * x, � x - ��������, ������������ �� 0 �� 1 (��
%������������), �� ���� [0, 1)
%������ x
x = 0:1/N:1-1/N;% �� ���� �� 0 �� 1-1/128 � ����� 1/128
z = 2*pi.*x; %������� z
%������ ������������
c0 = 0.2810639;
c1 = -0.5208972;
c2 = 0.1980399;
%������� ������������ ����
FTNI1.W = c0 + c1.*cos(z) + c2.*cos(2.*z);
%������� ����������� ������ �������
FTNI1.S = 20.*log10(fftshift(abs(fft(FTNI1.W))));

peaks = findpeaks(FTNI1.S);
if ~isempty(peaks)
    for i=1:length(peaks)
        if(peaks(i)<0)
            FTNI1.PSLL = max(peaks(i:round(end/2)));
            break;
        end
    end
else
    FTNI1.PSLL = 0;
end


%%
%��������� ������� ����
N = 256; %����� ����
r = 0.5; %����������� ����������
x = 0:1/N:1-1/N; %�������������� x ��� ��� ���������� ����� ����
z = zeros(1,length(x)); %������ ����� ������ z
for i=1:length(x)
    if(x(i)<r)
        z(i) = 2*pi.*x(i)/r;
    else
        break;
    end
end
%������� ������������ ����
FTNI2.W = c0 + c1.*cos(z) + c2.*cos(2.*z);
%������� ����������� ������ �������
FTNI2.S = 20.*log10(fftshift(abs(fft(FTNI2.W))));

peaks = findpeaks(FTNI2.S);
if ~isempty(peaks)
    for i=1:length(peaks)
        if(peaks(i)<0)
            FTNI2.PSLL = max(peaks(i:round(end/2)));
            break;
        end
    end
else
    FTNI2.PSLL = 0;
end
%%
%�������� ������� ���������� ����
figure;
plot(FTNI1.S);
grid on;

figure;
plot(FTNI2.S);
grid on;