function [W3db, emax, PSLL, S, NENBW, r, AF, PF, OC, S1, S2, ROV] = getWindowProperties(W,N,K)


S2 = sum(W.^2);
S1 = sum(W);

NENBW = N*S2/S1^2;


L = K*N;


S = abs(fft(W,L));

S = 20.*log10(S./S(1));



for i=1:length(S)
    
    if(S(i) < -3)
        break;
    end
    
end

W3db = 2*(i-1)/K;

e(1) = max(S(1:K/2));
e(2) = min(S(1:K/2));

[~,idx] = max(abs(e));

emax =  e(idx);
% emax = 0.5;

peaks = findpeaks(S);
if ~isempty(peaks)
    for i=1:length(peaks)
        if(peaks(i)<0)
            PSLL = max(peaks(i:round(end/2)));
            break;
        end
    end
else
    PSLL = 0;
end



[r,AF,PF,OC,ROV] = getOverlapParameters(W,N);

end